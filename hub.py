from bluetooth.ble import DiscoveryService

print("creating service...")
service = DiscoveryService()
print("service created...")
print("start discovering...")
devices = service.discover(2)
print("discovering finished!")
print("results:\n-----------\n")

for address, name in devices.items():
    print("name: {}, address: {}".format(name, address))